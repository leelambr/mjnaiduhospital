# Generated by Django 2.2.3 on 2020-02-26 09:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('NJnaiduapp', '0011_auto_20200226_1456'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chairman',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='chairman',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='contactus',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='contactus',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='ehs',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='ehs',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='empanelment',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='empanelment',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='ourhospital',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='ourhospital',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='successstories',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='successstories',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='testimonial',
            name='created_user',
        ),
        migrations.RemoveField(
            model_name='testimonial',
            name='modified_user',
        ),
        migrations.RemoveField(
            model_name='images',
            name='aclrehab',
        ),
        migrations.RemoveField(
            model_name='images',
            name='empanelment',
        ),
        migrations.RemoveField(
            model_name='images',
            name='successstories',
        ),
        migrations.DeleteModel(
            name='AclRehab',
        ),
        migrations.DeleteModel(
            name='Chairman',
        ),
        migrations.DeleteModel(
            name='ContactUs',
        ),
        migrations.DeleteModel(
            name='Ehs',
        ),
        migrations.DeleteModel(
            name='Empanelment',
        ),
        migrations.DeleteModel(
            name='OurHospital',
        ),
        migrations.DeleteModel(
            name='Successstories',
        ),
        migrations.DeleteModel(
            name='Testimonial',
        ),
    ]
