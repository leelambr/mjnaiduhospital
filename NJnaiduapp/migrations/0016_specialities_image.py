# Generated by Django 2.2.3 on 2020-02-26 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NJnaiduapp', '0015_auto_20200226_1629'),
    ]

    operations = [
        migrations.AddField(
            model_name='specialities',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='specialities/'),
        ),
    ]
